<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version='1.0' xmlns:xsl ='http://www.w3.org/1999/XSL/Transform'>
<xsl:template match='/'>
<html>
<body>
<h1>Test Report</h1>
<table cellpadding="10" cellspacing="1" border="2" style='table-layout:fixed'>
<xsl:for-each select='testsuites/testsuite'>
<tr><td>TEST SUITE:</td>
<td style='color:red'><b><xsl:value-of select='@name'/>
</b>
</td>
</tr>
<tr ><td>Test case</td>
<td>Status</td>
<td>Result</td>
<td>Time</td>
<td>Classname</td>
</tr>
<xsl:for-each select='testcase'>
<xsl:for-each select='failure'>
<xsl:if test="@message">
<tr><td>Failed test:</td></tr>
</xsl:if>
</xsl:for-each>
<tr>
<td><xsl:value-of select='@name'/></td>
<td><xsl:value-of select='@status'/></td>
<td><xsl:value-of select='@result'/></td>
<td><xsl:value-of select='@time'/></td>
<td><xsl:value-of select='@classname'/></td>
</tr>
</xsl:for-each>
</xsl:for-each>


</table>
</body>
</html>
</xsl:template>
</xsl:stylesheet>
